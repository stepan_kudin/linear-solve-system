TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp \
    reflections_method.cpp \
    jacobi.cpp

HEADERS += \
    reflections_method.h \
    jacobi.h

