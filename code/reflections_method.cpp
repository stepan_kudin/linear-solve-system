#include "reflections_method.h"


// See K. Yu. Bogachev book, page 57
int get_reflection_vector(double *matrix, int size_matrix, double *reflection_vector, int position)
{
  int index;
  double s_k = 0.0;
  double norm_a_1;
  double x_1;
  double norm_x_k;

  int k = position;
  ++position;

  for(index = position; index < size_matrix; ++index)
    s_k += matrix[index * size_matrix + k] * matrix[index * size_matrix + k];

  norm_a_1 = sqrt(matrix[k * size_matrix + k] * matrix[k * size_matrix + k] + s_k);

  x_1 = matrix[k * size_matrix + k] - norm_a_1;

  norm_x_k = sqrt(x_1 * x_1 + s_k);

//  std::cout << "x_1 = " << x_1 << " norm_x_k = " << norm_x_k << std::endl;

  if(fabs(norm_x_k) < EPS)
    return 1;

  reflection_vector[k] = x_1 / norm_x_k;
  for(index = position; index < size_matrix; ++index)
    reflection_vector[index] = matrix[index * size_matrix + k] / norm_x_k;

  return 0;
}

// See K. Yu. Bogachev book, page 54
int multiplacate_reflection_vector_and_vector(double *vector, int size_vector, int size_matrix, double *reflection_vector)
{
  int index;
  double scalar_product = 0.0;

  for(index = 0; index < size_vector; ++index)
    scalar_product += vector[index * size_matrix] * reflection_vector[index];

  for(index = 0; index < size_vector; ++index)
    vector[index * size_matrix] -= 2.0 * reflection_vector[index] * scalar_product;

  return 0;
}

int multiplacate_reflection_vector_and_matrix(double *matrix, int size_matrix, double *reflection_vector, int position)
{
  int index;

  for(index = position; index < size_matrix; ++index)
    multiplacate_reflection_vector_and_vector(matrix + index + position * size_matrix,
                                              size_matrix - position, size_matrix, reflection_vector + position);

  return 0;
}

int multiplacate_reflection_vector_and_adjoint_matrix(double *matrix, int size_matrix, double *reflection_vector, int position)
{
  int index;

  for(index = 0; index < size_matrix; ++index)
    multiplacate_reflection_vector_and_vector(matrix + index + position * size_matrix,
                                              size_matrix - position, size_matrix, reflection_vector + position);

  return 0;
}

int reflections_method_solver(double *matrix, int size_matrix, double *right_hand_member, double *result)
{
  int index_1, index_2, index;
  double b;

  double *reflection_vector = new double[size_matrix];

  for(index = 0; index < size_matrix - 1; ++index)
    {
      if(get_reflection_vector(matrix, size_matrix, reflection_vector, index))
        continue;

      multiplacate_reflection_vector_and_matrix(matrix, size_matrix, reflection_vector, index);
      multiplacate_reflection_vector_and_vector(right_hand_member + index, size_matrix - index, 1, reflection_vector + index);
    }

  for(index_1 = size_matrix - 1; index_1 >= 0; --index_1)
    {
      b = 0.0;
      for(index_2 = index_1 + 1; index_2 < size_matrix; ++index_2)
        b += matrix[index_1 * size_matrix + index_2] * result[index_2];

      result[index_1] = (right_hand_member[index_1] - b) / matrix[index_1 * size_matrix + index_1];
    }

  delete[] reflection_vector;

  return 0;
}

double residual(double *matrix, int size_matrix, double *right_hand_member, double *result)
{
  int index_1, index_2;
  double residual = 0.0;
  double *alternative_right_hand_member = new double[size_matrix];

  for(index_1 = 0; index_1 < size_matrix; ++index_1)
    alternative_right_hand_member[index_1] = 0.0;

  for(index_1 = 0; index_1 < size_matrix; ++index_1)
    {
      for(index_2 = 0; index_2 < size_matrix; ++index_2)
          alternative_right_hand_member[index_1] += matrix[index_1 * size_matrix + index_2] * result[index_2];
    }

  for(index_1 = 0; index_1 < size_matrix; ++index_1)
    residual += (right_hand_member[index_1] - alternative_right_hand_member[index_1]) *
        (right_hand_member[index_1] - alternative_right_hand_member[index_1]);

  residual = sqrt(residual);

  delete[] alternative_right_hand_member;

  return residual;
}

int reflections_method_inverse_matrix(double *matrix, int size_matrix, double *result)
{
  int index_1, index_2, index;
  double b;
  int count = size_matrix * size_matrix;

  // Init result matrix.
  for(index_1 = 0, index_2 = size_matrix; index_1 < count; ++index_1)
    {
      if(index_2 == size_matrix)
        {
          index_2 = 0;
          result[index_1] = 1.0;
        }
      else
        {
          ++index_2;
          result[index_1] = 0.0;
        }
    }

  double *reflection_vector = new double[size_matrix];

  count = size_matrix - 1;
  for(index = 0; index < count; ++index)
    {
      if(get_reflection_vector(matrix, size_matrix, reflection_vector, index))
        continue;

      multiplacate_reflection_vector_and_matrix(matrix, size_matrix, reflection_vector, index);
      multiplacate_reflection_vector_and_adjoint_matrix(result, size_matrix, reflection_vector, index);
    }

  for(index_1 = count; index_1 >= 0; --index_1)
    {
      b = matrix[index_1 * size_matrix + index_1];

      if (fabs(b) < EPS)
        return -1;

      matrix[index_1 * size_matrix + index_1] /= b;
      for(index_2 = 0; index_2 < size_matrix; ++index_2)
        result[index_1 * size_matrix + index_2] /= b;

      for(index_2 = 0; index_2 < index_1; ++index_2)
        {
          b = matrix[index_2 * size_matrix + index_1];

          matrix[index_2 * size_matrix + index_1] -= b * matrix[index_1 * size_matrix + index_1];
          for(index = 0; index < size_matrix; ++index)
            result[index_2 * size_matrix + index] -= b * result[index_1 * size_matrix + index];
        }
    }

  delete[] reflection_vector;

  return 0;
}
