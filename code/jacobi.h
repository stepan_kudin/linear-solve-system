#ifndef JACOBI_H
#define JACOBI_H

#include <iostream>
#include <math.h>

// Решение линейной системы итерационным методом Якоби
// Матрица в формате MSR - сначала главная диагональ, потом построчно,
// без элемента на главной диагонали, плюс массив индексов
// ВНИМАНИЕ! ДАННАЯ РЕАЛИЗАЦИЯ РАБОТАЕТ ТОЛЬКО ДЛЯ СИММЕТРИЧНЫХ МАТРИЦ
int jacobi_method_solver(double *matrix, int *matrix_index, int size_matrix, int size_line,
                         double *right_hand_member, double *result);

void multiplication_matrix_and_vector(double *matrix, int *matrix_index, int size_matrix, int size_line,
                                      double *vector, double *result);

// Точность метода Якоби
const double JACOBI_EPS = 1e-8;

#endif // JACOBI_H
