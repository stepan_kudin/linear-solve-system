#include "jacobi.h"

int jacobi_method_solver(double *matrix, int *matrix_index, int size_matrix, int size_line,
                         double *right_hand_member, double *result)
{
  double *r = new double[size_matrix];
  double *b = new double[size_matrix];
  double *buffer = new double[size_matrix];
  int index;
  double norm_r;
  double tau;
  double scalar_product_1, scalar_product_2, scalar_product_3;

  // Заполним матрицу обратную предобуславливателю B
  for(index = 0; index < size_matrix; ++index)
    b[index] = 1.0 / matrix[index];

  // Начальное приближение
  for(index = 0; index < size_matrix; ++index)
    result[index] = 0;

  // r = Au
  multiplication_matrix_and_vector(matrix, matrix_index, size_matrix, size_line, result, r);

  // Вычисляем невязку: r = f - r
  for(index = 0; index < size_matrix; ++index)
    r[index] = right_hand_member[index] - r[index];

  // Вычисляем норму невязки r
  norm_r = 0.0;
  for(index = 0; index < size_matrix; ++index)
    norm_r += r[index] * r[index];
  norm_r = sqrt(norm_r);

  while(norm_r > JACOBI_EPS)
    {
      // r = B^-1 r
      multiplication_matrix_and_vector(b, 0, size_matrix, 0, r, buffer);

      for(index = 0; index < size_matrix; ++index)
        r[index] = buffer[index];

      // tau = ((f, r) - (Ar, u)) / (Ar, r)

      // (f, r)
      scalar_product_1 = 0.0;
      for(index = 0; index < size_matrix; ++index)
        scalar_product_1 += right_hand_member[index] * r[index];

      // (Ar, u)
      multiplication_matrix_and_vector(matrix, matrix_index, size_matrix, size_line, r, buffer);

      scalar_product_2 = 0.0;
      for(index = 0; index < size_matrix; ++index)
        scalar_product_2 += buffer[index] * result[index];

      // (Ar, r)
      scalar_product_3 = 0.0;
      for(index = 0; index < size_matrix; ++index)
        scalar_product_3 += buffer[index] * r[index];

      tau = (scalar_product_1 - scalar_product_2) / scalar_product_3;

      // u = u + tau * r
      for(index = 0; index < size_matrix; ++index)
        result[index] += tau * r[index];

      // r = Au
      multiplication_matrix_and_vector(matrix, matrix_index, size_matrix, size_line, result, r);

      // Вычисляем невязку: r = f - r
      for(index = 0; index < size_matrix; ++index)
        r[index] = right_hand_member[index] - r[index];

      // Вычисляем норму невязки r
      norm_r = 0.0;
      for(index = 0; index < size_matrix; ++index)
        norm_r += r[index] * r[index];
      norm_r = sqrt(norm_r);

      std::cout << norm_r << std::endl;
    }

  delete[] r;
  delete[] b;
  delete[] buffer;

  return 0;
}

void multiplication_matrix_and_vector(double *matrix, int *matrix_index, int size_matrix, int size_line,
                                      double *vector, double *result)
{
  int index_1, index_2;
  int position;

  for(index_1 = 0; index_1 < size_matrix; ++index_1)
    {
      result[index_1] = matrix[index_1] * vector[index_1];

      for(index_2 = 0; index_2 < size_line; ++index_2)
        {
          position = index_1 * size_line + index_2;
          result[index_1] += vector[matrix_index[position]] * matrix[size_matrix + position];
        }
    }
}
