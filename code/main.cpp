#include <iostream>
#include <math.h>

#include "reflections_method.h"
#include "jacobi.h"

double func(int i, int j)
{
  return fabs(i - j);
}

int main()
{
  double *matrix = new double[9];
  double *result = new double[9];

  matrix[0] = 0.0;
  matrix[1] = 0.0;
  matrix[2] = 1.0;
  matrix[3] = -1.0;
  matrix[4] = 0.0;
  matrix[5] = 0.0;
  matrix[6] = 0.0;
  matrix[7] = 1.0;
  matrix[8] = 0.0;


  reflections_method_inverse_matrix(matrix, 3, result);

  for(int index_1 = 0; index_1 < 3; ++index_1)
    {
      for(int index_2 = 0; index_2 < 3; ++index_2)
        std::cout << result[index_1 * 3 + index_2] << " ";
      std::cout << std::endl;
    }

  delete[] matrix;
  delete[] result;

  return 0;
}

